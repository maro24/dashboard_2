import 'package:customgauge/customgauge.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  double sliderValue = 0;
  TextStyle valueTextStyle = TextStyle(color: Colors.white, fontSize: 28.0);

  TextStyle determineTempColor() {
    TextStyle result;
    if (this.sliderValue >= 0 && this.sliderValue <= 60) {
      result = TextStyle(color: Colors.lightBlueAccent, fontSize: 28.0);
    } else if (this.sliderValue > 60 && this.sliderValue <= 100) {
      result = TextStyle(color: Colors.green, fontSize: 28.0);
    } else if (this.sliderValue > 100 && this.sliderValue <= 110) {
      result = TextStyle(color: Colors.orange, fontSize: 28.0);
    } else if (this.sliderValue > 110 && this.sliderValue <= 120) {
      result = TextStyle(color: Colors.red, fontSize: 28.0);
    } else {
      result = TextStyle(color: Colors.white, fontSize: 28.0);
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(),
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text('Home'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            child: CustomGauge(
              minValue: 0,
              maxValue: 120,
              gaugeSize: 300,
              segments: [
                GaugeSegment('Low', 60, Colors.lightBlueAccent),
                GaugeSegment('Medium', 40, Colors.green),
                GaugeSegment('High', 10, Colors.orange),
                GaugeSegment('Extreme', 10, Colors.red),
              ],
              currentValue: sliderValue,
              valueWidget: Text(
                sliderValue.toInt().toString(),
                style: valueTextStyle,
              ),
              startMarkerStyle: TextStyle(color: Colors.white, fontSize: 28.0),
              endMarkerStyle: TextStyle(color: Colors.white, fontSize: 28.0),
              needleColor: Colors.white,
              displayWidget: Text('Temp',
                  style: TextStyle(color: Colors.white, fontSize: 28.0)),
            ),
          ),
          Container(
            child: Slider(
              activeColor: Colors.amber,
              inactiveColor: Colors.amber.withOpacity(.2),
              min: 0,
              max: 120,
              divisions: 120,
              onChanged: (value) {
                setState(() {
                  this.sliderValue = value;
                  valueTextStyle = determineTempColor();
                });
              },
              value: sliderValue,
            ),
          )
        ],
      ),
    );
  }
}
